<?php
include('connection.php');
if(isset($_POST['subapp']))
{
    $application_id=$_POST['appid'];
    $application_dob=$_POST['dob'];
    
    echo $application_id.$application_dob;
    
}

?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>The West Bengal Central School Service Commission</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
  <link href="css/layout.css" type="text/css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<header><img src="images/logo.png" alt=""></header>	
<div class="greenish-bg">ONLINE APPLICATION FORM FOR 1ST STATE LEVEL SELECTION TEST FOR RECRUITMENT OF ASSISTANT TEACHERS, 2016<br /> IN  UPPER PRIMARY LEVEL CLASSES IN GOVT. AIDED/SPONSORED SCHOOLS (EXCEPT HILL REGION)</div>    

<div class="bs-example reprint-page">
	   <div class="panel panel-primary">
	   			
			<div class="panel-heading">
				<h3 class="panel-title">Please enter your Application ID. and Date of Birth to reprint your Online Form for SLST, 2016 </h3>
			</div>
   
			<div class="panelbody">
			<div class="medium_text_normal1">This section is for those candidates who have already filled-up the Online Application Form and want to reprint their Printed Form.</div>

			<form  method="post" name="reprintfrm" autocomplete="off" class="form-horizontal form-horizontal1">
			<h4>REPRINT FORM</h4>			
			<div class="form-group">
            <label class="control-label col-md-4" for="form_no">ENTER THE 16-DIGIT APPLICATION NUMBER :</label>
            <div class="col-md-8">
			<input id="refno" name="appid" type="numbert" class="form-control" maxlength="16" onkeypress="return numbersonly(event)">
			</div>
			</div>
			
			<div class="form-group">
            <label class="control-label col-md-4" for="dob">ENTER YOUR DATE OF BIRTH:</label>
            <div class="col-md-8">							
			<input type="text" name="dob" id="dob" class="form-control"> [Format : MM/DD/YYYY]							
			</div>
			</div>
									
			<div class="form-group text-center">
            <div class="col-md-12">
			<input type="submit" name="subapp" id="rpsubmit" value="Submit" class="btn btn-primary">
			</div>
			</div>
			</form>
<div class="err"><img src="images/chu.gif" align="absmiddle"> <span>No match found.</span></div>

		</div>
	</div>
</div>

<div id="footer">&copy; 2019. all right reserved. <a href="http://westbengalssc.com/sscorg/wbssc/home/">WBCSSC</a>.</div>

	
        <div class="clear"></div>
</body>
</html>