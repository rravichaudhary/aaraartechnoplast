<?php
ob_start(); // needs to be added here
?>
<?php
include('connection.php');
$apperror='0';
if(isset($_POST['subapp']))
{
    $application_id=$_POST['appid'];
    $application_dob=$_POST['dob'];
                $queryfinal="select * from check_data_validate where APPLICATION_ID='".$application_id."' && DOB='".$application_dob."'";
                $res=mysqli_query($conn,$queryfinal);
                $totalfinal=mysqli_num_rows($res);
               if($totalfinal>0)
               {
                header("Location: printform.php?appid=" .base64_encode($application_id) . "");
                }else
                    {
                   $apperror='1';
                    }
}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>The West Bengal Central School Service Commission</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="shortcut icon" href="images/favicon.ico" type="image/x-icon">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="css/font-awesome.min.css" type="text/css">
    <link href="css/jquery.fancybox.css" rel="stylesheet" type="text/css">

  <link href="css/layout.css" type="text/css" rel="stylesheet">
<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body>
<header><div class="col-md-4"><img src="images/logo.png" alt=""></div>
<div class="col-md-8"><strong>The West Bengal Central School Service Commission</strong><br /> "Acharya Sadan", Bidhannagar, Sector - II, EE - 11 & 11/1, Kolkata - 700091, West Bengal.</div>
</header>	
<div class="greenish-bg">Intimation Letter for Verification (2nd Phase) of testimonials in c/w 1st SLST (at), 2016 <br />(upper-primary level except physical education and work education)</div>    

<div class="bs-example reprint-page">
	   <div class="panel panel-primary">
	   			
			<div class="panel-heading">
				<h3 class="panel-title">Please enter your Application ID and Date of Birth </h3>
			</div>
   
			<div class="panelbody">
			<!-- <div class="medium_text_normal1">This section is for those candidates who have already filled-up the Online Application Form and want to reprint their Printed Form.</div> -->

		<form  method="post" name="reprintfrm" autocomplete="off" class="form-horizontal form-horizontal1">
		<!--	<h4>PRINT FORM</h4>	-->
			<div class="form-group">
            <label class="control-label col-md-4" for="form_no">ENTER THE 16-DIGIT APPLICATION NUMBER :</label>
            <div class="col-md-8">
			<input id="refno" name="appid" type="text" class="form-control" maxlength="16" id="extra7" name="extra7" onkeypress="return isNumber(event)"">[Format : Enter Numbers only]
			</div>
			</div>
			
			<div class="form-group">
            <label class="control-label col-md-4" for="dob" placeholder="MM/DD/YYYY">ENTER YOUR DATE OF BIRTH:</label>
            <div class="col-md-8">							
			<input type="text" name="dob" id="test1" oninput="validateNumber(this);" class="form-control"> [Format : MM/DD/YYYY]							
			</div>
			</div>
									
			<div class="form-group text-center">
            <div class="col-md-12">
			<input type="submit" name="subapp" id="rpsubmit" value="Submit" class="btn btn-primary">
			</div>
			</div>
			</form>
		
<?php if($apperror=='1'){ ?>
<div class="err"><img src="images/chu.gif" align="absmiddle"> <span>ENTERED DETAILS DOES NOT MATCH OR YOU ARE NOT CALLED FOR SECOND PHASE VERIFICATION </span></div>
<?php } ?>


		</div>
	</div>
</div>

<div id="footer">&copy; 2019. all right reserved. <a href="http://westbengalssc.com/sscorg/wbssc/home/">WBCSSC</a>.</div>


<script src="js/jquery.min.js" type="text/javascript"></script>   
<script src="js/bootstrap.min.js" type="text/javascript"></script>     
<script src="js/jquery.fancybox.pack.js" type="text/javascript"></script>       
<script src="js/custom.js" type="text/javascript"></script>
<script>
    function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
var validNumber = new RegExp(/^\d*\/?\d*\/?\d*$/);
var lastValid = document.getElementById("test1").value;
function validateNumber(elem) {
  if (validNumber.test(elem.value)) {
    lastValid = elem.value;
  } else {
    elem.value = lastValid;
  }
}
</script>
</body>
</html>


