<?php 
session_start();
if(isset($_SESSION['m_number']))
{
include('header_vmit.php');
include('sidebar_vmit.php');
include('connection.php');
 $adminid=base64_decode($_GET['id']);
  $query="select * from admin where id='$adminid' and role='111'";
  $res=mysqli_query($conn,$query);
  while($row=mysqli_fetch_assoc($res))
    {
      $name=$row['name'];
      $assign_roll=$row['assign_roll'];
      $f_roll=$row['from_roll'];
       $opassword=$row['password'];
      $status1=$row['status'];
      $status=$row['status'];
     if($status==1){$status="Active";}else
     {$status="Deactive";}
      $created_on=$row['created_on'];
      $mobile=$row['mobile_no'];
      //$projects=$row['adv_no'];
    }

if(isset($_POST['update']))
{
  $name= $_POST['name'];
  $assign_roll=$_POST['assignroll'];
  $mobile=  $_POST['mobile'];
  $status=  $_POST['status'];
  //status 111 admin when that super admin status 222
 $query="update admin set name='$name',mobile_no='$mobile',status='$status',role='111' where id='$adminid'";
  if(mysqli_query($conn,$query))
  {
  echo '<script>window.location.href = "adminlist.php";</script>';
  }
}
?>
<div class="content-wrapper">
  <section class="content-header">
      <h1> EDIT ADMIN </h1>
      <ol class="breadcrumb"><li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li><li><a href="adminlist.php"><i class="fa fa-dashboard"></i> Admin Details</a></li><li class="active">Edit Admin</li></ol>  
        </section>
<section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">Admin Change Profile</h3>
      <a href="adminlist.php" title="Back" class="btn btn-default btn-xs pull-right"><i class="fa fa-caret-square-o-left fa-lg"></i> Back</a>
        </div>
        <div class="box-body">
          <div class="row">
      <form  method="POST" enctype="multipart/form-data">
            <div class="col-md-6">
              <div class="form-group">
                <label>User Name (<span style="color:#FF0000;">*</span>)</label>
                <input type="text" name="name" id="user_name" maxlength="100" class="form-control" value="<?= $name; ?>" required="">                      
        <p class="help-block"></p>
              </div>
              <div class="form-group">
                <label>Mobile No (<span style="color:#FF0000;">*</span>)</label>
                <input type="text" name="mobile" id="user_name" maxlength="100" class="form-control" value="<?= $mobile; ?>" required="">                      
        <p class="help-block"></p>
              </div>
              
              <div class="form-group">
          <label>Status</label>
          <span class="center-block">
            <input type="radio" name="status" value="1" <?php if($status1==1){ ?>checked="checked" <?php } ?> >Active
            <input type="radio" name="status" value="0" <?php if($status1==0){ ?>checked="checked" <?php } ?>>Inactive 
          </span>
        </div>
        
        <div class="col-md-6">
              
         
         <div class="form-group">
        <button class="btn btn-primary" id="form_submit" type="submit" name="update">Update</button>
         </div>
            </div>
                  
      </form>
          </div>
        </div>
      </div>
      </section>
      </div>
   <?php
  include('footer_vmit.php');
}
else
{
  echo '<script>window.location.href = "logout.php";</script>';
}?>