
//Used on Homepage
$(".owl-carousel").owlCarousel({
	items: 2,
	loop: true,
	rewindNav: false,
	autoplay:false,
	autoplayHoverPause: true,
	margin: 0,
	dots: false,
	//navText: ["<", ">"],
	responsive:{
		0:{ // breakpoint from 0 up - small smartphones
			items:1,
			nav:true,
			loop:false
		},
		480:{  // breakpoint from 480 up - smartphones // landscape
			items:2,
			nav:true,
			loop:false
		},
		768:{ // breakpoint from 768 up - tablets
			items:3,
			nav:true,
			loop:false
		},
		992:{ // breakpoint from 992 up - desktop
			items:4,
			nav:true,
			loop:false
		}
	}

});


$(".owl-carousel-location").owlCarousel({
	items: 2,
	loop: true,
	rewindNav: false,
	autoplay:false,
	autoplayHoverPause: true,
	margin: 0,
	dots: false,
	//navText: ["<", ">"],
	responsive:{
		0:{ // breakpoint from 0 up - small smartphones
			items:2,
			nav:true,
			loop:false
		},
		480:{  // breakpoint from 480 up - smartphones // landscape
			items:3,
			nav:true,
			loop:false
		},
		768:{ // breakpoint from 768 up - tablets
			items:4,
			nav:true,
			loop:false
		},
		992:{ // breakpoint from 992 up - desktop
			items:5,
			nav:true,
			loop:false
		}
	}

});


$(".owl-carousel-type").owlCarousel({
	items: 2,
	loop: true,
	rewindNav: false,
	autoplay:false,
	autoplayHoverPause: true,
	margin: 0,
	dots: false,
	//navText: ["<", ">"],
	responsive:{
		0:{ // breakpoint from 0 up - small smartphones
			items:2,
			nav:true,
			loop:false
		},
		480:{  // breakpoint from 480 up - smartphones // landscape
			items:3,
			nav:true,
			loop:false
		},
		768:{ // breakpoint from 768 up - tablets
			items:4,
			nav:true,
			loop:false
		},
		992:{ // breakpoint from 992 up - desktop
			items:5,
			nav:true,
			loop:false
		}
	}

});


 $( ".owl-prev").html('<i class="fa fa-chevron-left"></i>');
 $( ".owl-next").html('<i class="fa fa-chevron-right"></i>');


//Top-bar Sliding
$(document).ready(function(){
    $(".btn-slide-toggle a").click(function(){
        $(".topbar").slideToggle();
    });
});
//Top-bar Sliding
$(document).ready(function(){
    $(".filter-show").click(function(){
        $(".filter").slideToggle();
    });
});

$(".closefilter").click(function(){
    $(".filter").slideUp();
});

/*
$(".featured-name h3").text(function(){   
    return $(this).text().slice(0,13);
});
*/


//For Homepage testimonial ticker
(function() {

    var news = $(".testi");
    var newsIndex = -1;
    
    function showNextNews() {
        ++newsIndex;
        news.eq(newsIndex % news.length)
            .fadeIn(1000)
            .delay(3000)
            .fadeOut(1000, showNextNews);
    }
    
    showNextNews();
    
})();

$(document).ready(function(){
    $(".link-forgot").click(function(){
        $(".hide1").slideToggle();
    });
}); 


//Clone
$( ".max-guest, .right-col .list-price" ).clone().appendTo( ".guest-price" );	

//Range Slider
$("#perplate-budget").slider({ min: 0, max: 100, value: [0, 100], focus: true });
//new Slider("#ex16b", { min: 0, max: 10, value: [0, 10], focus: true });



//Login & Register
$("#join-trigger").click(function(){
  $('#login').modal('hide');
  $('#register').modal('show');
});

$("#login-trigger").click(function(){
  $('#register').modal('hide');
  $('#login').modal('show');
});

//Text Truncate;
$('.featured-name h3').text(function(i, text) {
    var t = $.trim(text);
    if (t.length > 10) {
        return $.trim(t).substring(0, 15) + "...";
    }
    return t;
});

$(document).ready(function() {
    // Configure/customize these variables.
    var showChar = 90;  // How many characters are shown by default
    var ellipsestext = "...";
    var moretext = "Show more >";
    var lesstext = "Show less";
    

    $('.desc-venue').each(function() {
        var content = $(this).html();
 
        if(content.length > showChar) {
 
            var c = content.substr(0, showChar);
            var h = content.substr(showChar, content.length - showChar);
 
            var html = c + '<span class="moreellipses">' + ellipsestext+ '&nbsp;</span><span class="morecontent"><span>' + h + '</span>&nbsp;&nbsp;<a href="" class="morelink">' + moretext + '</a></span>';
 
            $(this).html(html);
        }
 
    });
 
    $(".morelink").click(function(){
        if($(this).hasClass("less")) {
            $(this).removeClass("less");
            $(this).html(moretext);
        } else {
            $(this).addClass("less");
            $(this).html(lesstext);
        }
        $(this).parent().prev().toggle();
        $(this).prev().toggle();
        return false;
    });
});



function cambiar_login() {
  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_login";  
document.querySelector('.cont_form_login').style.display = "block";
document.querySelector('.cont_form_sign_up').style.opacity = "0";               

setTimeout(function(){  document.querySelector('.cont_form_login').style.opacity = "1"; },400);  
  
setTimeout(function(){    
document.querySelector('.cont_form_sign_up').style.display = "none";
},200);  
  }

function cambiar_sign_up(at) {
  document.querySelector('.cont_forms').className = "cont_forms cont_forms_active_sign_up";
  document.querySelector('.cont_form_sign_up').style.display = "block";
document.querySelector('.cont_form_login').style.opacity = "0";
  
setTimeout(function(){  document.querySelector('.cont_form_sign_up').style.opacity = "1";
},100);  

setTimeout(function(){   document.querySelector('.cont_form_login').style.display = "none";
},400);  


}    



function ocultar_login_sign_up() {

document.querySelector('.cont_forms').className = "cont_forms";  
document.querySelector('.cont_form_sign_up').style.opacity = "0";               
document.querySelector('.cont_form_login').style.opacity = "0"; 

setTimeout(function(){
document.querySelector('.cont_form_sign_up').style.display = "none";
document.querySelector('.cont_form_login').style.display = "none";
},500);  
  
  }
  
  
 