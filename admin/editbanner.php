<?php 
session_start();
if(isset($_SESSION['m_number']))
{
	
function dataready($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
} 
	
include('header_vmit.php');
include('sidebar_vmit.php');
include('connection.php');

  $id=base64_decode($_GET['id']);

  
   $query="select * from banner where id='$id' "; 
  $res=mysqli_query($conn,$query);
  while($row=mysqli_fetch_assoc($res))
    {
		
      $title=$row['name'];
	  $sub=$row['sub'];
      $cms_content=  $row['description'];
	  $banner=  $row['banner'];
	  
      $status1=$row['status'];
      $status=$row['status'];
      if($status==1){$status="Active";}else
      {$status="Deactive";}
    
       //$projects=$row['adv_no'];
       }

if(isset($_POST['update']))
{
     $title=$_POST['title'];
     $subt=$_POST['subt'];
     $description=$_POST['cms_content'];
	 

	 
	$file_name=$_FILES['file']['name'];
    $a=explode(".",$file_name);
	$ext=end($a);
	$img_name=rand().".".$ext;
	if(move_uploaded_file($_FILES['file']['tmp_name'],"../files/banner/$img_name"))
	{ $path=$img_name;  }
	else
	 {   
	  // while( $row = $db->fetch_array() )
      // {
	    $path= $banner;			
	 //  }
	  
	 }
	 
	 
	 
	 
	 
  
  
   $status=  $_POST['status']; 
  //status 111 admin when that super admin status 222
  
// $q="update cms set cms_title='".$cms_title."',meta_keyword='".$meta_keyword."',meta_description='".$meta_description."',cms_name='".$cms_name."',cms_content='".$cms_name."', status='".$status."', where cms_id='$cms_id' ";  exit;
 $query="update banner set name='$title',sub='$subt',description='$description',banner='$path', status='$status' where id='$id'";  


  
  if(mysqli_query($conn,$query))
  {
  echo '<script>window.location.href = "bannerlist.php";</script>';
  }
}
?>
<div class="content-wrapper">
  <section class="content-header">
      <h1> EDIT CMS </h1>  
      <ol class="breadcrumb"><li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li><li><a href="cmslist.php"><i class="fa fa-dashboard"></i> CMS Details</a></li><li class="active">Edit CMS</li></ol>  
        </section>
<section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">CMS Update</h3>
      <a href="adminlist.php" title="Back" class="btn btn-default btn-xs pull-right"><i class="fa fa-caret-square-o-left fa-lg"></i> Back</a>
        </div>
        <div class="box-body">
          <div class="row">
      <form  method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
            
            <div class="form-group">
                <label>Cms Title (<span style="color:#FF0000;">*</span>)</label>
                <input type="text" name="title" id="title" maxlength="100" class="form-control" value="<?= $title; ?>" required="">                      
                <p class="help-block"></p>
            </div>
            
           
             <div class="form-group">
                <label>Banner Sub Title (<span style="color:#FF0000;">*</span>)</label>
                <input type="text" name="subt" id="subt" maxlength="100" value="<?= $sub; ?>" class="form-control"  required="">                      
                <p class="help-block"></p>
            </div>
              
              
             <div class="form-group">
                <label>Cms Content (<span style="color:#FF0000;">*</span>)</label>
               <textarea id="cms_content" name="cms_content"><?= $cms_content; ?></textarea>                    
                <p class="help-block"></p>
                <script type="text/javascript">
	            CKEDITOR.replace('cms_content');
                </script>
             </div> 
              
              
            
            
              
             
             
          <div class="form-group">
                <label>Banner (<span style="color:#FF0000;">*</span>)</label>
                <input type="file" name="file" />   <br /> 
                
                <img src="../files/banner/<?= $banner; ?>" width="100px" height="50px" />         
        <p class="help-block"></p>
              </div>   
             
             
              
       <div class="form-group">
          <label>Status</label>
          <span class="center-block">
            <input type="radio" name="status" value="1" <?php if($status1==1){ ?>checked="checked" <?php } ?> >Active
            <input type="radio" name="status" value="0" <?php if($status1==0){ ?>checked="checked" <?php } ?>>Inactive 
          </span>
        </div>
        
        <div class="col-md-6">
              
         
         <div class="form-group">
        <button class="btn btn-primary" id="form_submit" type="submit" name="update">Update</button>
         </div>
            </div>
                  
      </form>
          </div>
        </div>
      </div>
      </section>
      </div>
   <?php
  include('footer_vmit.php');
}
else
{
  echo '<script>window.location.href = "logout.php";</script>';
}?>