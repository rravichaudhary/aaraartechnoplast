<?php 
session_start();
if(isset($_SESSION['m_number']))
{
	
function dataready($data) {
	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);
	return $data;
} 
	
include('header_vmit.php');
include('sidebar_vmit.php');
include('connection.php');

  $client_id=base64_decode($_GET['id']);

  
  $query="select * from product where p_id='$client_id' ";
  $res=mysqli_query($conn,$query);
  while($row=mysqli_fetch_assoc($res))
    {
      $client_name=$row['name'];
      $client_image=$row['image'];

	  
      $status1=$row['status'];
      $status=$row['status'];
      if($status==1){$status="Active";}else
      {$status="Deactive";}
    
       //$projects=$row['adv_no'];
       }

if(isset($_POST['update']))
{
     $client_name=$_POST['client_name'];
     $product=$_POST['product'];
	 $file_name=$_FILES['file']['name'];
     $a=explode(".",$file_name);
	 $ext=end($a);
	 $img_name=rand().".".$ext;
	 if(move_uploaded_file($_FILES['file']['tmp_name'],"../files/clients/$img_name"))
	 {  $path=$img_name;      }
	  else
	 {   
	  //while( $row = $db->fetch_array() )
      // {
	    // $path= $row['image'];			
	  // }
	  
	   $path = $client_image;
	  
	 }

   $status=  $_POST['status']; 

  $query="update product set name='$client_name',product_type='$product',image='$path', status='$status' where p_id='$client_id'";  

  if(mysqli_query($conn,$query))
  {
  echo '<script>window.location.href = "productlist.php";</script>';
  }
}
?>
<div class="content-wrapper">
  <section class="content-header">
      <h1> EDIT CMS </h1>
      <ol class="breadcrumb"><li><a href="dashboard.php"><i class="fa fa-dashboard"></i> Home</a></li><li><a href="cmslist.php"><i class="fa fa-dashboard"></i> CMS Details</a></li><li class="active">Edit CMS</li></ol>  
        </section>
<section class="content">
      <div class="box box-default">
        <div class="box-header with-border">
          <h3 class="box-title">CMS Update</h3>
      <a href="adminlist.php" title="Back" class="btn btn-default btn-xs pull-right"><i class="fa fa-caret-square-o-left fa-lg"></i> Back</a>
        </div>
        <div class="box-body">
          <div class="row">
      <form  method="POST" enctype="multipart/form-data">
            <div class="col-md-12">
            
            
            <div class="form-group">
                <label>Product Type (<span style="color:#FF0000;">*</span>)</label>  
                <select name="product" class="form-control" required="">
                 <option value="">Plese Select</option>
                 <option value="Product Range">Product Range</option>
                 <option value="Product">Product</option>
              </select>
                                     
                <p class="help-block"></p>
            </div>
            
            
            
            <div class="form-group">
                <label>Client Name (<span style="color:#FF0000;">*</span>)</label>
                <input type="text" name="client_name" id="client_name" maxlength="100" class="form-control" value="<?= $client_name; ?>" required="">                      
                <p class="help-block"></p>
            </div>
              
              
             <div class="form-group">
                <label>Client Logo (<span style="color:#FF0000;">*</span>)</label>
                <input type="file" name="file" />  <br />
                
                <img src="../files/clients/<?= $client_image; ?>" width="120px" height="100px" />
                             
        <p class="help-block"></p>
              </div> 
              
              
              
              <div class="form-group">
          <label>Status</label>
          <span class="center-block">
            <input type="radio" name="status" value="1" <?php if($status1==1){ ?>checked="checked" <?php } ?> >Active
            <input type="radio" name="status" value="0" <?php if($status1==0){ ?>checked="checked" <?php } ?>>Inactive 
          </span>
        </div>
        
        <div class="col-md-6">
              
         
         <div class="form-group">
        <button class="btn btn-primary" id="form_submit" type="submit" name="update">Update</button>
         </div>
            </div>
                  
      </form>
          </div>
        </div>
      </div>
      </section>
      </div>
   <?php
  include('footer_vmit.php');
}
else
{
  echo '<script>window.location.href = "logout.php";</script>';
}?>