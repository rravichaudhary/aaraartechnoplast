<?php 
include('connection.php');
//session_start();
if(isset($_SESSION['m_number']))
{
   $mobile=$_SESSION['m_number'];
  $query="select * from admin where mobile_no='".$mobile."'";
  $res=mysqli_query($conn,$query);
  while($row=mysqli_fetch_assoc($res))
    {
      $role=$row['role'];
      $name=$row['name'];
       $adv_id=base64_encode($row['adv_no']);
       $redictpath=$row['url'];
      
      
    }
  ?>
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="image/1120090818.png" class="img-circle" alt="User Image">
        </div>
        
        <div class="pull-left info">
          <p><?= ucwords($name); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
     
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">MAIN NAVIGATION</li>
        
        <li>
          <a href="passwordchange.php">
            <i class="fa fa-th"></i> <span>Change Password</span>
            <span class="pull-right-container">
             
            </span>
          </a>
        </li>
        <li>
          <a href="admindashboard.php">
            <i class="fa fa-th"></i> <span>Dashboard</span>
            <span class="pull-right-container">
              
            </span>
          </a>
        </li>
        
        
        
        <li>
          <a href="candidateslist.php">
            <i class="fa fa-th"></i> <span>All Candidates List</span>
            <span class="pull-right-container">
             
            </span>
          </a>
        </li>
        
        <li>
          <a href="admincandidateslist.php">
            <i class="fa fa-th"></i> <span>Change Candidates List</span>
            <span class="pull-right-container">
             
            </span>
          </a>
        </li>
        
        
        
        
       
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>
  <?php 
}
else
{
  echo '<script>window.location.href = "logout.php";</script>';
}?>