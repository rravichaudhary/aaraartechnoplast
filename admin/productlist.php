<?php 
session_start();
if(isset($_SESSION['m_number']))
{
include('header_vmit.php');
include('sidebar_vmit.php');
?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
       PRODUCT DETAILS
      </h1>
      <ol class="breadcrumb">
        <li><a href="dashboard"><i class="fa fa-dashboard"></i> Home</a></li>
        <li class="active">Product Details</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="row">
        <div class="col-xs-12">
          <!-- /.box -->

          <div class="box">
            <div class="box-header">
                  <h3 class="box-title">Product list</h3>

          <a href="createproduct.php" class="btn btn-default btn-xs pull-right"><i class="fa fa-fw fa-plus"></i> Add</a>                </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>Product Name</th>
                  <th>Product Image</th>
                  <th>Status</th>
                  <th>Action</th>
                </tr>
                </thead>
                <tbody>
                  <?php
                  include('connection.php');
  $query="select * from product where status='1'";
  $res=mysqli_query($conn,$query);
  $s=1;
  while($row=mysqli_fetch_assoc($res))
    {
      $name=$row['name'];
      //$banner=$row['banner'];

     
      $p_id=base64_encode($row['p_id']);
	  //$id=$row['cms_id'];
      $status=$row['status'];
     if($status==1){$status="Active";}else
     {$status="Inactive";}
     
 ?>     
                      
                          <tr>
                              <td><?= ucwords($name); ?></td>
                              <td><img src="../files/clients/<?php echo $banner=$row['image']; ?>" width="100px" height="50px" /></td>
                              
                              <td><?= $status; ?></td>
                  <td><a href="editproduct.php?id=<?= $p_id; ?>" class="btn btn-default btn-xs"><i class="fa fa-edit"></i> Edit</a>
                   &nbsp;<a href="deleteproduct.php?id=<?= $p_id; ?>" class="btn btn-primary btn-xs"><i class="fa fa-edit"></i>Delete</a>
                  
</td>
                </tr>
                <?php } ?>
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
        <!-- /.col -->
      </div>
      <!-- /.row -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  

  
  <?php
  include('footer_vmit.php');
}
else
{
  echo '<script>window.location.href = "logout.php";</script>';
}?>