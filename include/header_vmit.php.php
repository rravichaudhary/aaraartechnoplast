<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
	<meta http-equiv="x-ua-compatible" content="ie=edge">
	<meta name="description" content="EduRead - Education HTML5 Template">
	<meta name="keywords" content="college, education, institute, responsive, school, teacher, template, university">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>AarAar Technolpast</title> 
	<link rel="shortcut icon" href="images/favicon.png" type="image/x-icon">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/assets/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="css/assets/font-awesome.min.css">
	<!-- Popup -->
	<link href="css/assets/magnific-popup.css" rel="stylesheet"> 
	<!-- Revolution Slider -->
	<link rel="stylesheet" href="css/assets/revolution/layers.css">
	<link rel="stylesheet" href="css/assets/revolution/navigation.css">
	<link rel="stylesheet" href="css/assets/revolution/settings.css">	
    <!-- Google Fonts -->    
    <link href="https://fonts.googleapis.com/css?family=Montserrat:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
    <!-- Slick Slider -->
	<link href="css/assets/slick.css" rel="stylesheet"> 	
	<link href="css/assets/slick-theme.css" rel="stylesheet"> 
    <link href="fonts/font-awesome.min.css" rel="stylesheet" />
	<!-- Mean Menu-->
	<link rel="stylesheet" href="css/assets/meanmenu.css">   
	<!-- Custom CSS -->
	<link rel="stylesheet" href="css/style.css">
	<link rel="stylesheet" href="css/responsive.css">    
</head>
<body>
<!-- Preloader -->
<div id="preloader">
	<div id="status">&nbsp;</div>
</div>    
    <div style="height:14px;width:100%;background-color:#a53010"></div>
<header id="header" class="inner-page">
	<div class="header-body">
		<nav class="navbar edu-navbar" style="z-index:99!important">
			<div class="container">
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
						<span class="sr-only">Toggle navigation</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
					</button>
					<a href="index.html" class="navbar-brand  data-scroll"><img src="images/logo1.png" alt=""></a>
				</div>

				<div class="collapse navbar-collapse edu-nav main-menu" id="bs-example-navbar-collapse-1">
					<ul class="nav navbar-nav pull-right">
						<li class="active"><a data-scroll="" href="about-us.html">ABOUT Us</a>
							<!-- dropdwon start -->
                            <ul class="dropdown list-unstyled">
                                <li><a href="about-us.html">Overview</a></li>
                                <li><a href="vision-mission.html">Vision &amp; Mission</a></li>
                                <li><a href="journey-so-far.html">Journey so Far</a></li>
                                <li><a href="management.html">Management</a></li>
                                <li><a href="award-achievements.html">Awards &amp; Achievements</a></li>
                                <li><a href="csr.html">CSR</a></li>
                            </ul>
                            <!-- dropdown end -->
						</li>
                        <li>
                            <a data-scroll="" href="why-us.html">Why us</a>
                        </li>
						<li><a data-scroll="" href="#">Products</a>
						<ul class="dropdown list-unstyled">
                               <li><a href="2-wheels-products.html">2 Wheels Products</a></li>
                                <li><a href="4-wheels-products.html">4 Wheels Products</a></li>
                                <li><a href="lcv-hcv-products.html">LCV/ HCV Products</a></li>
                                <li><a href="domestic-appliances.html">Domestic Appliances</a></li>
                            </ul>
						
						</li>
						<li><a data-scroll="" href="infrastructure.html">infrastructure</a>
                            <ul class="dropdown list-unstyled">
                                <li><a href="infrastructure.html">Overview</a></li>
                                <li><a href="facilities.html">Facilities</a></li>
                                <li><a href="machinery.html">Machinery</a></li>
                                <li><a href="tool-room.html">Tool Room</a></li>
                                <li><a href="logistic.html">Logistics</a></li>
                            </ul>
						</li>
                        <!--<li>
                            <a data-scroll href="policy.html">Policy</a>
                        </li>-->
                        <li>
                            <a data-scroll="" href="career.html">Career</a>
                            <ul class="dropdown list-unstyled">
                                <li><a href="career.html">Overview</a></li>
                                <li><a href="current-opening.html">Current Openings</a></li>
                                <li><a href="apply-now.html">Apply Now</a></li>
                            </ul>
                        </li>
                        <li>
                            <a data-scroll="" href="contact-us.html">Contact us</a>
                        </li>
					</ul>
				</div><!-- /.navbar-collapse -->
			</div><!-- /.container -->
		</nav>
	</div>	
    
    
    
    
    
    
    
    
    <!--==================Inner Banner===================-->
    <div class="inner-banner">
    <img src="images/inner-banner.jpg" alt="">   
    <div class="bradcrumb">
				<div class="col-md-12">
					<div class="intro-text">
						<h1>Vision Mission</h1>
                         <span class="line"></span>	
						<p><span><a href="">Home <i class="fa fa-angle-right"></i></a></span> <span class="b-active">Vision Mission</span></p>
					</div>
				</div>
			</div>   
    </div>
   <!--  End Inner Banner-->
</header>