﻿<?php include 'header.php';
 include('connection.php'); 
 
 $query = "select * from cms where cms_id='53'"; 
 $res = mysqli_query($conn,$query );
 while($row=mysqli_fetch_assoc($res))
 {
	 $cms_name=$row['cms_name'];
     $cms_content=$row['cms_content'];
     $banner=$row['banner'];
     $cms_title=$row['cms_title'];
 }
 
 
    
?>


    <!--==================Inner Banner===================-->
    <div class="inner-banner">
      <img src="files/banner/<?php echo $banner  ?>" alt="">  
    <div class="bradcrumb">
				<div class="col-md-12">
					<div class="intro-text">
						<h1> <?php echo $cms_name  ?></h1>
                         <span class="line"></span>	
						<p><span><a href="">Home <i class="fa fa-angle-right"></i></a></span> <span class="b-active"><?php echo $cms_name  ?></span></p>
					</div>	
				</div>
			</div>   
    </div>
   <!--  End Inner Banner-->
</header>
	<!--  End header section-->
<!-- Start Welcome Area section -->
<section class="about-section">
	<div class="container">	
	<h3 class="widget-title">Journey <span>So Far</span></h3>
	<br />
		<div class="row">
               <!-- <div class="col-sm-12 journey-section">				
          <div class="col-md-12">
<marquee behavior="scroll" direction="left" width="100%" height="250px" overflow="hidden" text-align="center">
    <?php echo $cms_content  ?>
    </marquee>
</div>
                
                </div>-->
                
                <div class="col-md-12">
                <div class="main-timeline2">
                    <div class="timeline">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">1978</h3>
                            <p class="description">
                                Small-scale manufacturing unit set up at Faridabad, Haryana.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">1994</h3>
                            <p class="description">
                                Expansion of plant from 2000 sq. ft. to 70000 sq. ft.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">1997</h3>
                            <p class="description">
                                Aar Aar commenced Business with the Automobile Sector.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">2000</h3>
                            <p class="description">
                                Converted to a full fledge Private Limited Company.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">2004</h3>
                            <p class="description">
                                ISO/TS 16949 Certification from BSI.<br /> ISO 14001 Certification by TCL.<br /> ISO 18001 Certification by TCL.
                            </p>
                        </a>
                    </div>
                    <div class="timeline">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">2010</h3>
                            <p class="description">
                                New production facilities setup at IMT Manesar, Haryana.
                            </p>
                        </a>
                    </div>
                         <div class="timeline">
                             <div class="mentery">
                        <span class="icon fa fa-globe"></span>
                        <a href="javascript:;" class="timeline-content">
                            <h3 class="title">2018</h3>
                            <p class="description">
                               Added IATF 16949-2016 & ISO 45001:2018 certifications to our portfolio.
                            </p>
                        </a>
                        </div>
                    </div>
                </div>
            </div>
<!-- Ends: . -->					
		</div>
	</div>
</section><!-- Ends: . -->



<?php 
include('footer.php'); ?>
    
    



    
