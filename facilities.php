﻿<?php include 'header.php';
 include('connection.php'); 
 $query = "select * from cms where cms_id='61'";
 $res = mysqli_query($conn,$query );
 while($row=mysqli_fetch_assoc($res)) {
	 $cms_name=$row['cms_name'];
     $cms_content=$row['cms_content'];
     $banner=$row['banner'];
     $cms_title=$row['cms_title'];
 }
?>

<!--==================Inner Banner===================-->

<div class="inner-banner"> <img src="files/banner/<?php echo $banner  ?>" alt="">
  <div class="bradcrumb">
    <div class="col-md-12">
      <div class="intro-text">
        <h1><?php echo $cms_name  ?></h1>
        <span class="line"></span>
        <p><span><a href="">Home <i class="fa fa-angle-right"></i></a></span> <span class="b-active"><?php echo $cms_name  ?></span></p>
      </div>
    </div>
  </div>
</div>

<!--  End Inner Banner-->

</header>

<!--  End header section--> 

<!-- Start Welcome Area section -->

<section class="about-section">
  <div class="container">
    <h3 class="widget-title"><?php echo $cms_name  ?></h3>
        <div class="row">
            <div class="col-md-6">
                <p class="text-justify">Aar Aar's both units, in Faridabad and Manesar have the latest PLC controlled moulding machines ranging from 80 tons to 1300 tons. The Faridabad unit is manned by 180 staff members (including 31 technical staff), who work round the clock, relentlessly to make sure we never lack behind. The second plant in Manesar was set up in 2010 with 40500 sq. ft. It was set up with the purpose of catering to increasing numerous industrial requirements, with a cost and time effective approach, as the logistics, quality and production teams came together to ensure the best quality.</p>
            </div>
            <div class="col-md-6">
                <p class="text-justify">
                    We also have well-equipped Tool Room is available in house for the maintenance of tools as well as to make new tools.
                    While doing all this we try to make sure we use the best of technology available to achieve lightweight but robust products that will not only lower cost but reduce pollution & lower Carbon Footprints, as we believe that global warming is a problem for everyone and everyone should be doing their part. This is our little step towards saving the world
                </p>
            </div> 
            </div><br />
    <div class="row">
      <div class="col-sm-12 Welcome-area-text"> <?php echo $cms_content  ?> </div>
    </div>
  </div>
  <!-- Ends: . -->
  
  </div>
  </div>
</section>
<!-- Ends: . -->

<?php 

include('footer.php'); ?>
