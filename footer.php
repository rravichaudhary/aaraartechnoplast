<!-- Footer Area section -->
<footer>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 footer-content-box">
				<div class="col-md-4 footer-col">
					<h3>Aar Aar Technoplast</h3>
					<!--<p>Aar Aar Technoplast Pvt. Ltd. was established in 1978, at Faridabad, India. From a very small unit, it has gone on to become a market leader in the field on Plastic Injection Moulded Components.</p>-->
					<ul class="list-unstyled">						
						<!--<li><span><i class="fa fa-map-marker footer-icon"></i></span>18/01, Mathura Road,<br /> Faridabad -121007<br />  Haryana(India)</li>-->
						<li><span><i class="fa fa-map-marker footer-icon"></i></span><strong>Head Office &amp; Plant I</strong><br>18/01, Mathura Road,<br> Faridabad -121007<br>  Haryana(India) <br> <br>
                            <strong>Plant II</strong><br>
                        Sector 8, IMT Manesar,<br> Gurugram -122051<br>  Haryana(India)
                        </li>
                        <li><span><i class="fa fa-phone footer-icon"></i></span>Tel: +91 129 2262000 / 22623000 / 2285000<br /> Fax: +91 129 2286666
                        </li>
                        <li><span><i class="fa fa-envelope footer-icon"></i></span>Email: <a href="sratra@aaraartechnoplast.com">sratra@aaraartechnoplast.com</a><br /><a href="mailto:vp@aaraartechnoplast.com">vp@aaraartechnoplast.com</a></li>
					</ul>
				</div>

				<div class="col-md-2 footer-col1">
					<h3>About Us</h3>
					<ul class="list-unstyled">
						<li><a href="about-us.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Overview</a></li>
						<li><a href="vision-mission.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Vision & Mission</a></li>
						<li><a href="journey-so-far.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Journey So Far</a></li>
						<li><a href="management.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Management</a></li>
						<li><a href="award-achievements.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Awards & Achievements</a></li>
                        <li><a href="javascript:;"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Customer</a></li>   
                        <!--<li><a href="csr.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>CSR</a></li>-->                        
					</ul>
				</div>	
     <div class="col-md-2 footer-col2">					
					<h3>Products</h3>
					<ul class="list-unstyled">
						<li><a href="2-wheels-products.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>2 Wheels Products</a></li>
						<li><a href="4-wheels-products.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>4 Wheels Products</a></li>
						<li><a href="lcv-hcv-products.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>LCV / HCV Products</a></li>
						<li><a href="domestic-appliances.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Domestic Appliances</a></li>
					</ul>
				</div>					

				<div class="col-md-2">
					<h3>Infrastructure</h3>
					<ul class="list-unstyled">
						<li><a href="why-us.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Why Us</a></li>
						<li><a href="infrastructure.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Overview</a></li>
                        <li><a href="facilities.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Facilities</a></li>
						<li><a href="machinery.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Machinery</a></li>
						<li><a href="tool-room.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Tool Room</a></li>
						<li><a href="logistic.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Logistics</a></li>
					</ul>
				</div>	
                <div class="col-md-2">
                    <h3>Contact Us <!--Career--></h3>
                    <ul class="list-unstyled">
                        <!--<li><a href="career.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Overview</a></li>
                        <li><a href="current-opening.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Current Openings</a></li>
                        <li><a href="apply-now.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Apply Now</a></li>-->
                        <li><a href="career.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Career</a></li>
                        <li><a href="sitemap.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Site Map</a></li>
                        <li><a href="privacy-policy.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Privacy Policy</a></li>
                        <li><a href="disclaimer.php"><span><i class="fa fa-long-arrow-right footer-icon"></i></span>Disclaimer</a></li>
                    </ul>
                </div>
			</div>
		</div>
	</div>

	<div class="footer-bottom">
		<div class="container">
			<div class="footer-bottom-inner">
				<div class="row">
					<div class="col-md-6 col-sm-12 footer-no-padding">
						<p>&copy; Copyright 2019 <a target="_blank" href="javascript:;">AarAar Technoplast</a> | All rights reserved</p>
					</div>
					<div class="col-md-6 col-sm-12 footer-no-padding">
                        <div class="right-text pull-right"><p>Design &amp; Developed by <a target="_blank" href="http://www.virtualmindinfotech.com/">VMIT</a></p></div>
					</div>
				</div>
			</div>
		</div>
	</div><!-- ./ End footer-bottom -->		
</footer><!-- ./ End Footer Area-->


    <!-- ============================
    JavaScript Files
    ============================= -->
    <!-- jQuery -->
	<script src="js/vendor/jquery-1.12.4.min.js"></script>
	<!-- Bootstrap JS -->
	<script src="js/assets/bootstrap.min.js"></script>
	 <!-- owl carousel -->
    <script src="js/assets/owl.carousel.min.js"></script>
 	<!-- Revolution Slider -->
	<script src="js/assets/revolution/jquery.themepunch.revolution.min.js"></script>
	<script src="js/assets/revolution/jquery.themepunch.tools.min.js"></script>   
	<!-- Popup -->
    <!--<script src="js/assets/jquery.magnific-popup.min.js"></script>-->
    <!-- Sticky JS -->
	<script src="js/assets/jquery.sticky.js"></script>
    <script src="js/assets/jquery.magnific-popup.min.js"></script>
	<!-- Counter Up -->
    <script src="js/assets/jquery.counterup.min.js"></script>
    <script src="js/assets/waypoints.min.js"></script>
   <!-- Slick Slider-->
    <script src="js/assets/slick.min.js"></script>
    <!-- Main Menu -->
	<script src="js/assets/jquery.meanmenu.min.js"></script>
	<!-- Revolution Extensions -->
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.actions.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.carousel.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.kenburn.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.layeranimation.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.migration.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.navigation.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.parallax.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.slideanims.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/extensions/revolution.extension.video.min.js"></script>
	<script type="text/javascript" src="js/assets/revolution/revolution.js"></script>
	<!-- Custom JS -->
    <script src="js/custom.js"></script>
    	
</body>
</html>
